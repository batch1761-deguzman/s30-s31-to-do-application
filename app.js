// [SECTION] Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
// [SECTION] Server Set up
		// establish a connection
const app = express();
	// define a path/address
const port = 4000; // assign port to port 4000 to avoid conflict  in the fufure wtg front end app that runs on port 3000

// [SECTION] Database Connection
	// Connect to MongoDB Atlas
	// change password bracket to your own password
	// change myFirstDatabase to toDo176(new database name). will auto create a new database if database does not exist
	mongoose.connect('mongodb+srv://jdeguzman:admin123@cluster0.ogj65.mongodb.net/toDo176?retryWrites=true&w=majority',{

		useNewUrlParser: true, // remove deprecation warnings
		useUnifiedTopology: true

	});

	/*Create notification if the connection to the db is a success or a failure*/
	let db = mongoose.connection;
	/*Lets add an on() method from our mongoose connection to show if the connection
	has succeeded or failed in both the terminal and in the browser for our client.*/
	db.on('error',console.error.bind(console,"Cinnection Error"));
	/*once the connection is open and successful, we will output a message in the
	terminal:*/
	db.once('open',() => console.log('Connected to MongoDB'));

		/*Middleware - a middleware, in expressjs context, are methods, functuibs that
		acts and adds features to our application*/

	app.use(express.json());

	/*Schema*/
	/*Before we can create documents from our api to save into our database, we first have to
	determine the structure of the documents to be written in our database.
	This is to ensure consistency of our documents and avoids future errors.*/

	// Schema acts as a blueprint from our data/documents

	// Schema() constructor from mongoose to create a bew scgema object
	const taskSchema = new mongoose.Schema({
		/*
			Define the fields for our docmuents.
			we will also be able to determine data type of the values
		*/

		name: String,
		status: String
	});

	// Mongoose Model
	/*
		Models are used to connect to your api to the corresponding collection in your
		datbase. It is a representation of your collection,

		Models uses schemas to creaste object that correspond to the schema. By default,
		when creating the colection from your model, the collection name is pluralized.

		mongoose.model(<nameofCollectionInAtlas>,<schemaToFollow>)
	*/

	const Task = mongoose.model('task',taskSchema);

	// post route to create a new task
	app.post('/tasks',(req,res) => {

		/*when creating a new post/put or any route that requires data from the client,
		first console log your req body or any part of the reques that contains the
		data*/

		/*Creating a new task document by isong the constructor of our task model*/
		console.log(req.body);

		let newTask = new Task({

			name: req.body.name,
			status: req.body.status
		})

		// .save() method from an object created by a model.
		// save( method will allow us to save our document by conencting to our collection via our model)

		// 

		// save() has 2 approaches:
		// 1. add an anonymous function to handle the created document or error
		// 2. we cab add .then() chain which will allow us to handle errors and created document in seperate functions
		
		/*newTask.save((error,savedTask) => {

			if (error) {
				res.send('error docu');
			} else {
				res.send(savedTask);

			}
		})*/


		// then() and catch() chain
		/*Then() is used to handle the propper result/ returned vaile of a function. if the 
		function properly returns a value, we can get a separate function to handle it/*/
		/*.catch() is used to handle catch the error from the use of a function. so that if an error 
		occurs, we can get handle the error seperately*/

		newTask.save()
		.then(result => res.send({message: "Document Creation Successful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));

	});

	// GET Method Request to retrieve ALL task Documents from our Colelction

	app.get('/tasks',(req,res) => {

		/*TO query using mongoose, first access the modeol of the collection you want to
		manipulate*/
		// mongodb - db.tasks.find({})
		Task.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));

	});

	const sampleSchema = new mongoose.Schema({
		name: String,
		isActive: Boolean
	});

	const Sample = mongoose.model('samples',sampleSchema);

	app.post('/samples',(req,res) => {

		let newSample = new Sample({

			name: req.body.name,
			isActive: req.body.isActive

		});

		newSample.save((error,savedSample) => {

			if (error) {
				res.send(error);
			} else {
				res.send(savedSample)
			}
		});

	});
	/*
	/sample
		Create a new Get request methoud rout to get ALL sample documents
		send the array of sample documents in our postman client
		Else catch the error and send the error in the client
	*/
	app.get('/samples',(req,res) => {

		Sample.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));

	});



	/*step by step*/

	// 1. Schema

	/*const manggagamitSchema = new mongoose.Schema({

		username: String,
		isAdmin: Boolean
	});

	const Manggagamit = mongoose.model('manggagamit',manggagamitSchema);

	app.post('/mgaManggagamit',(req,res) => {

		let newMangagamit =new Manggagamit({

			username: req.body.username,
			isAdmin: req.body.isAdmin

		});

		newMangagamit.save((savedManggagamit,error) => {
			if (error) {
				res.send(error);
			} else {
				res.send(savedManggagamit);
			};
		});
	});

*/

/*Activity*/
/*
			Create a new schema for User. It should have the following fields:
                username,password.
            The data types for both fields is String.

            Create a new model out of your schema and save it in a variable called User

            Create a new POST method route to create a new user document:
                -endpoint: "/users"
                -This route should be able to create a new user document.
                -Then, send the result in the client.
                -Catch an error while saving, send the error in the client.

            Create a new GET method route to retrieve all user documents:
                -endpoint: "/users"
                -Then, send the result in the client.
                -Catch an error, send the error in the client.
*/

const usersSchema = new mongoose.Schema({

	username: String,
	password: String
});

const User = mongoose.model('user',usersSchema);

app.post('/users',(req,res) => {

	let newUser = new User({

		username: req.body.username,
		password: req.body.password

	});

	newUser.save()
	.then(result => res.send({message: "User Account Successfully Created"}))
	.catch(error => res.send({message: "Error in User Creation"}));

	
});

app.get('/users',(req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

});



	// Get the credentials of your atlas user.

// [SECTION] Entry Point Response
	// binding the connecton to the designated port
	app.listen(port,() => console.log(`Server running at port ${port}`) );
