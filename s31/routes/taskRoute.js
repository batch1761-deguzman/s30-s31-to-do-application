// Conains all the endpoints for our Application
// We seperate the routes such that "app.js" only contains infomration on the server

const express = require("express")

// Create a router instance that functions as a middleware and routing system
// Allow access to HTTP method middlewares that makes it easier to create routes for out application
const router = express.Router();

const taskController = require("../controllers/taskControllers")

// [Routes]

// Route for getting all the tasks
router.get("/",(req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});

// Route is for creating a tasj
router.post("/",(req,res) => {
	taskController.createTask(req.body). then(resultFromController => res.send(resultFromController))
});

// Route for Deleting a task
	// call out the routing component to register a brand new endpoint
	// When integrating a path variable within the URI, you are also changing the behavior of th epath from STATIC to DYNAMIC.
	// 2 Types of End points
		// Static Route
			// - unchangging, fixedm constant, steady
		// Dynamic Route (add a colon to the  endpoint)
			// - interchangeable or NOT FIXED,
router.delete('/:task', (req, res) => {
	// identify the task to be executed within this endpoint
	// call out the proper function for this route and identify the srouce/provider of the function.
	// DETERMINE wether the value inside the path variable is transmitter to the server
	console.log(req.params.task);
	// place the value of the path variable inside its own container.

	let taskID = req.params.task;
	// res.send('Hello From Delete');// this is only to check if the set up is correct

	// retrieve the identity of the task by inserting the ObjectID of the resource.
	// make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as a path variable.
	// Path variable -> this will allow us to insert data within the scope of the URL. (:)
		// what is a variable? -> container, storage of information
	// When is a path variable useful?
		// - when inserting only a single piece of information
		// - this is also usefil when passing down infromation to REST API method that does NOT include a BODY section like 'GET'

	// upon executing this method, a promise will be initialized. so we need to handle the result of the promise in our route module.
	taskController.deleteTask(taskID).then(resultNgDelete => res.send(resultNgDelete));
});

// Route for updating task status (Pending to Complete)
	// Let's create a dynamic endpoint for this brand new route
router.put('/:task', (req, res) => {
	// Check if you are able to aquire the values inside the path varable.
	console.log(req.params.task);//check if the value inside the parameters of the route is properly transmitted to the server.
	let idNiTask = req.params.task; //this is declared to simplify the means of calling out the value of the path variable key.

	// identify the bussiness logic behind this task inside the controller module.
	// call the intended controller to execute the process. make sure to identify the provider/source of the function
	// after invoking the cotroller method, handle the outcome.
	taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));
});

// Route for Updating task status (Completed to Pending)
	// this will be a counter procedure for the previous task.
router.put('/:task/pending', (req, res) => {
	let id = req.params.task;
	// console.log(id); //test the innitial set up

	// declare the bussiness logic aspect of this brand new task in our application.
	// invoke the task you want to execute in this route.
	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	});
}); 

// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;